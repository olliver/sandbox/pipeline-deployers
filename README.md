# Pipeline deployers, a set of GitLab deploy templates
As GitLab does not have a [marketplace][marketplace] of sorts for deploy
related tasks, using a dedicated repo with a few includes is the
'next best thing'.


## Release branches
When working with git, a few git-flows speak of using release branches.
The `release-branch.yml` template offers just that. For more information see
the [release-branch.md][release-branch.md] documentation.


## Release announcement
To help with announcing a release using [GitLab's releases][gitlabrelease] a
simple template was added to help reduce some boilerplate. Once this feature is
more mature on GitLab's side, this template should not be needed anymore. For
more information see [announce-release.md][announce-release.md].


[marketplace]: https://gitlab.com/gitlab-org/gitlab/-/issues/276004
[gitlabrelease]: https://docs.gitlab.com/ee/ci/yaml/index.html#release
