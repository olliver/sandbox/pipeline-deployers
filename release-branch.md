# Release brancher
GitLab template to create release branches.


## Introduction
It is fairly common with various git strategies to have release branches.
While the action is simple, to just create a branch, this template helps
to automate things by only pushing a specific git tag, and thus the whole
process becomes automated. Automation may be needed, in that a first tag
on this release branch is desired (usually the first release-candidate).

There's several ways to do this, using git commands or the GitLab API.
The advantage of using the GitLab API is that there's no additional data
traffic (no extra git-pulls needed on shallow clones for example) and
authentication is easier handled with tokens as well.

Besides creating a release branch, by default an accompanying release candidate
tag is created, resulting in the following

```console
master
├── v1.0 (release branch tag)
└── release/v1.0 (release branch)
    └────── v1.0.0-rc1 (release candidate tag)
```

The reasoning behind these are the following. To very clearly indicate and
'fixate' the release branch point creation adding traceability.
The release branch in itself is used allow for potential evolution of releases
within that branch its scope. The used `release/` prefix is configurable but
helps with identifying release branches and grouping them in a familiar folder
like structure as is somewhat common with git branching.
Finally when creating a release branch, the intend is usually to prepare for
a release, and thus the first release candidate of that series. A flag exists
to control this behavior.


## Usage
To make use of the GitLab template, it needs to be first included.

```yaml
include:
  - project: 'ci-includes/pipeline-deployers'
    ref: 'master'
    rules:
      - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+$/'
    file: 'release-branch.yml'
```

The job itself is triggered by a so called release tag. A release tag is
defined by the **major** and **minor** components of a version. This is done
through a GitLab rule matching a regular expression and part of the include.

In addition to the above, a stage called `deploy` is needed which will cause
this job to run.

The release tag is in this example fixed using a regular expression, matching
for example `v0.1`.


## Configuration
The template can take some optional variables to set some options.


### RELEASE_BRANCHER_TOKEN
The variable `${RELEASE_BRANCHER_TOKEN}` may contain the API token to authenticate
against the API. Also known as the HTTP header `Private-Token`.

If unset, the variable `${CI_PERSONAL_TOKEN}` is used.


### RELEASE_BRANCHER_PREFIX
The variable `${RELEASE_BRANCHER_TOKEN}` may contain a branch prefix (any
acceptable `git branch` string) that is to be used as a prefix. This string
is prepended to the branch name, which in combination with `${CI_COMMIT_TAG}`
forms the full branch name.

If unset, the default `release/` is used.


### RELEASE_BRANCHER_POSTFIX
The variable `${RELEASE_BRANCHER_POSTFIX}` may contain a tag postfix (any
acceptable `git tag` string) that is used as a tag postfix. This string is
appended to the first tag on the release branch that is to be created.

If unset, the default `.0-rc1` is used.


### RELEASE_BRANCHER_NO_TAG
When `${RELEASE_BRANCHER_NO_TAG}` is set to any value, tag creation is skipped.


### RELEASE_BRANCHER_TAG_MESSAGE
Annotated tags in GitLab require a message to be set. Proper tags ought to be
annotated. As such, there must always be a message as part of the tag. The
message is derived normally from the tag that requests the branch to be
created. To override the default string, `${RELEASE_BRANCH_TAG_MESSAGE}` can
be set.

If unset, the default
`"See tag '${CI_COMMIT_TAG:?}'\n\n(Auto-created release candidate)` is used.


## Generate tagging tips
When wanting to put markdown in git tags, for example to store a changelog,
using markdown may be problematic, due to git by default treating the `#`
character as comment. The combination of the following git options are useful
in that case.

First, the `git commit --cleanup` option can be set to `scissors` via
`git config --global commit.cleanup scissors`.
Secondly, changing the git comment character so that git does not swallow
lines starting with a `#` by setting the commentChar. Any character can be
used, but `;` is recommended here, as this is not a likely character to end up
on the first column. This can be done via
`git config --global core.commentChar ";"`
