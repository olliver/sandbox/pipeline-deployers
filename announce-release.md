# Release announcer
GitLab template to announce release on the GitLab releases page.


## Introduction
GitLab's release feature and API are a powerful way to inform the world that
a release is ready for consumption. In theory GitLab supports releases via
the [release keyword][releasekeyword] but it is a bit of a kludge at this
moment, and cannot be used with annotated git tag messages as description.

To circumvent these issues, this template exists, hopefully only temporally,
but expected to last a few years.


> __Note:__ Asset links will not be supported until release-cli v0.11.0 to
> support handling json arrays.


## Usage
To make use of the GitLab template, it needs to be first included.

```yaml
include:
  - project: 'ci-includes/pipeline-deployers'
    ref: 'master'
    rules:
      - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+(\.\d+)+$/'
    file: 'announce-release.yml'
```

The job itself is triggered by a so called release tag. A release tag is
defined by the **major**, **minor** and **patch** components of a version.
This is done through a GitLab rule matching a regular expression and part of
the include.

In addition to the above, a stage called `complete` is needed which will cause
this job to run. The stage name is called `complete` and not `deploy` to offer
the option to ensure the announcement of the release is done after any `deploy`
job. E.g. to upload files somewhere.

The release tag is in this example fixed using a regular expression, matching
for example `v0.1.2` at the minimum.


### Release announcement delay
Release announcements are by default delayed by 86 minutes. While the time is
[arbitrarily][86ed] chosen, the reason it exists is to have a final window of
opportunity to cancel a potentially 'gone-bad' release. As no variables can
be set or used at this level of a job, the only way is to extend the
`announce_release` job and change or remove the delay from the rule.


## Configuration
The template can take some optional variables to set some options. The core
components however are all git(lab) built-ins. E.g. the `$CI_COMMIT_TAG` is
used to create a release against. The `$CI_COMMIT_TAG_MESSAGE` is used to set
the description from the tag content.


### RELEASE_ANNOUNCER_MILESTONES
The variable `${RELEASE_ANNOUNCER_MILESTONES}` is a white-space separated list
of milestone titles.

> __Note__ Whitespace includes newlines, but also regular tabs and spaces!

If unset, no milestones are linked.


### RELEASE_ANNOUNCER_NAME
The variable `${RELEASE_ANNOUNCER_NAME}` is can be used to override the name of
the release.

If unset, `${CI_PROJECT_TITLE} ${CI_COMMIT_TAG}` is used.


[releasekeyword]: https://docs.gitlab.com/ee/ci/yaml/index.html#release
[86ed]: https://en.wikipedia.org/wiki/86_(term)
